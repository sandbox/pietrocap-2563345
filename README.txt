INTRODUCTION
------------

The EU Cookie Law module aims to make your Drupal site full compliants with
the European and Italian law about use of cookies, which requires an opt-in
system, effectively meaning that the consumer must give his or her consent
before cookies are stored in their browser.

REQUIREMENTS
------------

This module depends on and so requires the following modules
- Dynamic Cache (https://www.drupal.org/project/dynamic_cache)
- Libraries API 7.x-2.x (http://drupal.org/project/libraries)
- Help (Core)
- EUCookieLaw plugin (https://github.com/diegolamonica/EUCookieLaw)

RECOMMENDED MODULES
-------------------
 - Modules Weight (https://www.drupal.org/project/modules_weight):
   when enabled, provide an admin interface to view and order the modules
   execution order. This way you can do some checks regarding the Dynamic Cache
   module.
   The main Dynamic Cache module must run AFTER all other modules
   (higher weight), while the Bootfix module must run BEFORE all other modules
   (lower weight).


INSTALLATION
------------

1. Download the EUCookieLaw plugin from https://github.com/diegolamonica/EUCookieLaw
2. Unzip the file and rename the folder to "eucookielaw" (pay attention to the
   case of the letters)
3. Delete all files and subfolders except for
    - eucookielaw.css
    - EUCookieLaw.js
    - eucookielaw-header.php
    - gzcompat.php
3. Put the folder in a libraries directory
    - Ex: sites/all/libraries
4. Install and enable all required modules
    - Dynamic Cache (https://www.drupal.org/project/dynamic_cache)
    - Libraries API 7.x-2.x (http://drupal.org/project/libraries)
    - Help (Core)
5. Put the EU Cookie Law module in your Drupal modules directory and enable it
   in admin/modules
6. Configure and use the module at admin/config/system/eucookielaw

CONFIGURATION
-------------

Configure the module at admin/config/system/eucookielaw, where you can enter a
list of disallowed domains and modify the behavour of the plugin. Ex.:
- The consent duration in days
- How to block cookies
- Some debug functions
- How to display the popup, agree on scroll, agree on clic, reload page, etc.
- Some final cleanups


-------------------------------------------------------------------------------
